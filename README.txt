﻿

*********************************************************************

README FOR SHELL
*********************************************************************

Minix Version : 3.2.1 
Compiler : cc 

Login Details for MINIX 3.2.1
**********************************************************************
login    : root
password : hello

Loading Program to minix:
**********************************************************************

1. Browse the .vmx file from VMware Player to open the Minix 3.2.1
2. Navigate to folder OSDI by typing "cd .." followed by "cd OSDI"
3. Invoke the shell by using "sh shell.sh" command 
4. Alternatively, sourcecode can be loaded into the OS by configuring ssh and pushing the file.
 use the below commands to configure ssh
	pkgin update
	pkgin install openssh
5. Set the password for user by typing passwd
6. Use an ftp client like fileZilla to connect to MINIX using ssh and transfer the file 
7. Go to the OSDI directory by typing "cd .." followed by "cd OSDI"
8. Invoke the shell by using "sh shell.sh" command 
9. On successful login, the custom shell will execute a profile file which will initialize the environment variables like HOME and PATH
      By Default: HOME variable is set to “/root”
                  PATH variable contains “/bin:/usr/bin”.
                  PROMPT variable is set to the current directory and will be changed when we navigate to other directory.
				  
		These values can be changed inside profile.txt
                  

10. The new custom shell can support all command like "ls", "ls -l", "cat filename" (All commands with and without argument)

11. The new custom shell can support redirection and piped operations also.
      
	Syntax
	Redirection   -- "command argument =>filename"
	Pipe command  -- " $wc $ (fgrep -l include MinixShell.c)
    

 
12. To create a new process, we use getpid where a fork() command is executed and  when we enter the  command “getpid”, shell will execute fork() to generate a new child process. 
	
	Eg:
	PROMPT> getpid
	the process id is 23456



13. The shell can execute pipe commands (commands with $) along with redirection command.

	Eg: 
	$wc -l => file1.txt $ ls

14. The shell can be terminated by using “exit” or by pressing ctrl -c. 

